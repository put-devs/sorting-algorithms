function insertionSort(arr) {
  let sortedIndex = 0;
  let swaps = 0;
  let comps = 0;
  while (sortedIndex < arr.length){
    let target = sortedIndex;
    let targetValue = arr[target];
    for (let i = sortedIndex - 1; i > -1; --i){
      ++comps;
      if (targetValue > arr[i]) {
        arr[i+1] = arr[i];
        ++swaps;
        --target;
      }
    }
    arr[target] = targetValue;
    //console.log('Posortowano: ' + arr.filter( (a, index)=> index <= sortedIndex));
    //console.log(`Zamiany: ${swaps} | Porownania: ${comps}`);
    sortedIndex++;
  }
  return {sortedArray: arr, comps: comps, swaps: swaps};
}

function quickSort(arr){
  if (arr.length < 2) return {sortedArray: arr, comps: 0, swaps: 0, pivots: []};
  let pivot = arr.pop();
  let comps = 0;
  let swaps = 0;
  let greater = [];
  let smaller = [];
  for(i=0;i<arr.length;++i){
    comps++;
    if (arr[i] > pivot) {
      greater.push(arr[i]);
      continue;
    }
    swaps++;
    smaller.push(arr[i]);
  }
  const {sortedArray : gSA, comps: gC, swaps: gS, pivots: gPivots} = quickSort(greater);
  const {sortedArray : sSA, comps: sC, swaps: sS, pivots: sPivots} = quickSort(smaller);
  return {sortedArray: gSA.concat([pivot].concat(sSA)),
  comps: comps + gC + sC, swaps: swaps + gS + sS,
  pivots: [pivot].concat(gPivots).concat(sPivots)};
}

function merge(arr1, arr2){
  let comps = 0;
  let arr = [];
  while(arr1.length && arr2.length){
    comps++;
    if (arr1[0] > arr2[0]) arr.push(arr1.shift());
    else arr.push(arr2.shift());
  }
  return {sortedArray: arr.concat(arr1.concat(arr2)), comps: comps, merges: 1};
}

function mergeSort(arr){
  if (arr.length < 2) return {sortedArray: arr, comps: 0, merges: 0};
  let slicer = Math.floor((arr.length)/2);
  const {sortedArray: leftSlice, comps: leftComps, merges: leftMerges} = mergeSort(arr.slice(0,slicer));
  const {sortedArray: rightSlice, comps: rightComps, merges: rightMerges} = mergeSort(arr.slice(slicer));
  const {sortedArray, comps, merges} = merge(leftSlice, rightSlice);
  return {sortedArray: sortedArray, comps: comps + leftComps + rightComps, merges: merges + leftMerges + rightMerges};
}

function genArray(n, policy){
  let min = 1;
  let max = 10*n;
  policies = [
  /*   policies
  0 -> RANDOM     1 -> ASCENDING
  2 -> DESCENDING 3 -> A-shaped
  4 -> V-shaped
  */
  function () {
    // 0 -> RANDOM
    // Math.random() nie zwraca nigdy 1, dlatego zeby wylosowanie 10*n bylo mozliwe
    // dodajemy 1 do wyniku (to przy okazji zapewnia minimalny wynik = 1)
    let arr = [];
    for(let i=0;i<n;++i){
      arr.push(Math.floor(Math.random() * (max) + 1));
    }
    return arr;
  },
  function () {
    // 1 -> ASCENDING
    let arr = policies[0]();
    return arr.sort((a, b) => a - b);
  },
  function () {
    // 2 -> DESCENDING
    let arr = policies[0]();
    return arr.sort((a, b) => b - a);
  },
  function () {
    // 3 -> A-shaped
    let arr = policies[2]();
    let direction = 0; //0-left, 1 right
    let ramps = [[],[]];
    while (arr.length > 0){
      if (direction===0)
        ramps[0].unshift(arr.shift());
      else
        ramps[1].push(arr.shift());
      direction = (direction)? 0 : 1;
    }
    return ramps[0].concat(ramps[1]);
  },
  function () {
    // 4 -> V-shaped
    let arr = policies[1]();
    let direction = 0; //0-left, 1 right
    let ramps = [[],[]];
    while (arr.length > 0){
      if (direction===0)
        ramps[0].unshift(arr.shift());
      else
        ramps[1].push(arr.shift());
      direction = (direction)? 0 : 1;
    }
    return ramps[0].concat(ramps[1]);
  }
];
  return policies[policy]();
}
function genTestArrays(ns, m){
  /*Mamy w tablicy ns 10 wartosci n, dla kazdej z nich generujemy 10 ciagow, wsrod ktorych losowo wybieramy
jak je tworzymy, niektore beda malejace, niektore rosnace, a niektore V-ksztaltne itd.
*/
  let arrays = [];
  ns.forEach(n => {
    arrays.push([]);
    for (let i = 0; i < m;++i){
      let policy = Math.floor(Math.random() * 5);
      arrays[arrays.length-1].push({array: genArray(n,policy), policy: policy});
    }
  });
  return arrays;
}

function heapSort(arr){
  let heap = [];
  let comps = 0;
  let swaps = 0;
  while (arr.length){
    heap.push(arr.pop());
    let newValuePos = heap.length-1;
    let parentPos = Math.floor((newValuePos-1)/2);
    while (newValuePos){
      ++comps;
      if (heap[parentPos] <= heap[newValuePos]) break;
      ++swaps;
      let temp = heap[newValuePos];
      heap[newValuePos] = heap[parentPos];
      heap[parentPos] = temp;
      newValuePos = parentPos;
      parentPos = Math.floor((newValuePos-1)/2);
    }
  }
  let sorted = [];
  while (true){
    sorted.unshift(heap.shift());
    if (!heap.length) return {sortedArray: sorted, comps: comps, swaps: swaps};
    heap.unshift(heap.pop());
    let root = 0;
    while (true){
      let left = 2*root+1;
      let right = 2*root+2;
      let min = root;
      comps+=2;
      if ((heap.length-1 >= right) && (heap[right] < heap[min]))
        min = right;
      if ((heap.length-1 >= left) && (heap[left] < heap[min]))
        min = left;
      if (min === root) break;
      ++swaps;
      let temp = heap[min];
      heap[min] = heap[root];
      heap[root] = temp;
      root = min;
    }
  }
}

function shellSort(arr){
  let gaps = [1];
  let k = 2;
  let comps = 0;
  let swaps = 0;
  while (gaps[gaps.length-1] < arr.length) {
    gaps.push(Math.floor((Math.pow(3, k) - 1)/2));
    ++k;
  }
  let outGaps = [];
  while (gaps.length){
    let gap = gaps.pop();
    outGaps.push(gap);
    for (let a = gap; a < arr.length; ++a){
      let right = a;
      let left = right - gap;
      while (true){
        ++comps;
        if (arr[left] >= arr[right]) break;
        ++swaps;
        let temp = arr[right];
        arr[right] = arr[left];
        arr[left] = temp;
        right -= gap;
        left -= gap;
        if (left < 0) break;
      }
    }
  }
  return {sortedArray: arr, comps: comps, swaps: swaps, gaps: outGaps};
}

let sortingAlgorithmsNames = ["Algorytmy Sortowania", "Merge Sort", "Heap Sort", "Insertion Sort", "Shell Sort", "Quick Sort"];
let sortingAlgorithms = [ undefined, mergeSort, heapSort, insertionSort, shellSort, quickSort];
implementations = [
  [],
  ["function merge(arr1, arr2){",
"  let comps = 0;",
"  let arr = [];",
"  while(arr1.length && arr2.length){",
"    comps++;",
"    if (arr1[0] > arr2[0]) arr.push(arr1.shift());",
"    else arr.push(arr2.shift());",
"  }",
"  return {sortedArray: arr.concat(arr1.concat(arr2)),",
"  comps: comps, merges: 1};",
"}",
"",
"function mergeSort(arr){",
"  if (arr.length < 2)",
"  return {sortedArray: arr, comps: 0, merges: 0};",
"  let slicer = Math.floor((arr.length)/2);",
"  const {sortedArray: leftSlice, comps: leftComps,",
"  merges: leftMerges} = mergeSort(arr.slice(0,slicer));",
"  const {sortedArray: rightSlice, comps: rightComps,",
"  merges: rightMerges} = mergeSort(arr.slice(slicer));",
"  const {sortedArray, comps, merges} =",
"  merge(leftSlice, rightSlice);",
"  return {sortedArray: sortedArray,",
"  comps: comps + leftComps + rightComps,",
"  merges: merges + leftMerges + rightMerges};",
"}"],
  ["function heapSort(arr){",
"  let heap = [];",
"  let comps = 0;",
"  let swaps = 0;",
"  while (arr.length){",
"    heap.push(arr.pop());",
"    let newValuePos = heap.length-1;",
"    let parentPos = Math.floor((newValuePos-1)/2);",
"    while (newValuePos){",
"      ++comps;",
"      if (heap[parentPos] <= heap[newValuePos]) break;",
"      ++swaps;",
"      let temp = heap[newValuePos];",
"      heap[newValuePos] = heap[parentPos];",
"      heap[parentPos] = temp;",
"      newValuePos = parentPos;",
"      parentPos = Math.floor((newValuePos-1)/2);",
"    }",
"  }",
"  let sorted = [];",
"  while (true){",
"    sorted.unshift(heap.shift());",
"    if (!heap.length) return {sortedArray: sorted,",
"	comps: comps, swaps: swaps};",
"    heap.unshift(heap.pop());",
"    let root = 0;",
"    while (true){",
"      let left = 2*root+1;",
"      let right = 2*root+2;",
"      let min = root;",
"      comps+=2;",
"      if ((heap.length-1 >= right)",
"	  && (heap[right] < heap[min]))",
"        min = right;",
"      if ((heap.length-1 >= left)",
"	  && (heap[left] < heap[min]))",
"        min = left;",
"      if (min === root) break;",
"      ++swaps;",
"      let temp = heap[min];",
"      heap[min] = heap[root];",
"      heap[root] = temp;",
"      root = min;",
"    }",
"  }",
"}"],
  ["function insertionSort(arr) {",
"  let sortedIndex = 0;",
"  let swaps = 0;",
"  let comps = 0;",
"  while (sortedIndex < arr.length){",
"    let target = sortedIndex;",
"    let targetValue = arr[target];",
"    for (let i = sortedIndex - 1; i > -1; --i){",
"      ++comps;",
"      if (targetValue > arr[i]) {",
"        arr[i+1] = arr[i];",
"        ++swaps;",
"        --target;",
"      }",
"    }",
"    arr[target] = targetValue;",
"    sortedIndex++;",
"  }",
"  return {sortedArray: arr, comps: comps, swaps: swaps};",
"}"],
["function shellSort(arr){",
"  let gaps = [1];",
"  let k = 2;",
"  let comps = 0;",
"  let swaps = 0;",
"  while (gaps[gaps.length-1] < arr.length) {",
"    gaps.push(Math.floor((Math.pow(3, k) - 1)/2));",
"    ++k;",
"  }",
"  while (gaps.length){",
"    let gap = gaps.pop();",
"    for (let a = gap; a < arr.length; ++a){",
"      let right = a;",
"      let left = right - gap;",
"      while (true){",
"        ++comps;",
"        if (arr[left] >= arr[right]) break;",
"        ++swaps;",
"        let temp = arr[right];",
"        arr[right] = arr[left];",
"        arr[left] = temp;",
"        right -= gap;",
"        left -= gap;",
"        if (left < 0) break;",
"      }",
"    }",
"  }",
"  return {sortedArray: arr, comps: comps, swaps: swaps};",
"}"],
["function quickSort(arr){",
"  if (arr.length < 2)",
"  return {sortedArray: arr, comps: 0, swaps: 0, pivots: []};",
"  let pivot = arr.pop();",
"  let comps = 0;",
"  let swaps = 0;",
"  let greater = [];",
"  let smaller = [];",
"  for(i=0;i<arr.length;++i){",
"    comps++;",
"    if (arr[i] > pivot) {",
"      greater.push(arr[i]);",
"      continue;",
"    }",
"    swaps++;",
"    smaller.push(arr[i]);",
"  }",
"  const {sortedArray : gSA, comps: gC, swaps: gS,",
"  pivots: gPivots} = quickSort(greater);",
"  const {sortedArray : sSA, comps: sC, swaps: sS,",
"  pivots: sPivots} = quickSort(smaller);",
"  return {sortedArray: gSA.concat([pivot].concat(sSA)),",
"  comps: comps + gC + sC, swaps: swaps + gS + sS,",
"  pivots: [pivot].concat(gPivots).concat(sPivots)};",
"}"]
];
