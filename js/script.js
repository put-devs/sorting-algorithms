function getTestResults(algo, arr, how=0){
	let testArray = [].concat(arr);
	let testOutput = '';
	let t1, t2, time;
	if (algo === 1){
		t1 = performance.now();
		const {sortedArray, comps, merges} = sortingAlgorithms[algo](testArray);
		t2 = performance.now();
		time = Math.floor((t2-t1)*100)/100;
		testOutput += how? `${time} ` : `Czas: ${time}ms<br>`;
		testOutput += how? `${comps} ` : `Porównania: ${comps}<br>`;
		testOutput += how? `${merges}` : `Scalenia: ${merges}<br>`;
	}
	else if (algo === 5){
		t1 = performance.now();
		const {sortedArray, comps, swaps, pivots} = sortingAlgorithms[algo](testArray);
		t2 = performance.now();
		time = Math.floor((t2-t1)*100)/100;
		testOutput += how? `${time} ` : `Czas: ${time}ms<br>`;
		testOutput += how? ` ${comps} ` : `Porównania: ${comps}<br>`;
		testOutput += how? `${swaps}` : `Zamiany: ${swaps}<br>`;
		//testOutput += `Wartości pivota: <span style="font-size: 5px">${pivots}</span><br>`;
	}
	else {
		t1 = performance.now();
		const {sortedArray, comps, swaps} = sortingAlgorithms[algo](testArray);
		t2 = performance.now();
		time = Math.floor((t2-t1)*100)/100;
		testOutput += how? `${time} ` : `Czas: ${time}ms<br>`;
		testOutput +=  how? ` ${comps} ` : `Porównania: ${comps}<br>`;
		testOutput += how? `${swaps}` : `Zamiany: ${swaps}<br>`;
	}
	return testOutput;
}
// place - current position in page (home or viewed sort)
let place = 0;
function homeButton() { place = 0; update(); }
function mergeButton() { place = 1; update(); }
function heapButton() { place = 2; update(); }
function insertionButton() { place = 3; update(); }
function shellButton() { place = 4; update(); }
function quickButton() { place = 5; update(); }


let userData = [];
let nsFromUser = [1500, 3000, 5500];
let hmFromUser = 10;
let algoLeft = document.querySelector(".algo-left");
let algorithmName = document.querySelector(".algorithm-name");
let input = document.querySelector("#data-input");
let data = document.querySelector("#data-output");
let output = document.querySelector("#sorted-output");
let dataInputText = document.querySelector(".data-input-text");
let particularAlgorithm = document.querySelector(".particular-algorithm");

let testingDiv = document.querySelector(".testing-div");
let nsInput = document.querySelector("#ns-input");
let hmInput = document.querySelector("#hm-input");
let testingNOutput = document.querySelector("#testing-n-output");
let nsInputText = document.querySelector("#ns-input-text");
let hmInputText = document.querySelector("#hm-input-text");
let testingOutput = document.querySelector(".output-container");
let opis = document.querySelector(".opis");
let rawData = document.querySelector('input[id="raw-data"]');
opisy = [undefined,
'<div><span class="float opis-margin">Merge sort (sortowanie przez scalanie) - algorytm sortowania stosujący metodę "dziel i zwyciężaj".<br><ol>Wyróżnić można 3 podstawowe kroki:<li>Podział zestawu danych na 2 równe części</li><li>Zastosowanie sortowania przez scalanie dla każdej z nich oddzielnie, chyba że pozostał już tylko 1 element</li><li>Połączenie posortowanych podciągów w 1 posortowany ciąg</li></ol></span><span class="info">Złożoność czasowa: <b>O(n*log(n))</b></span><br><span class="info">Złożoność pamięciowa: <b>O(n)</b></span><br></div>',
'<div><span class="float opis-margin">Heap sort (sortowanie przez kopcowanie) - algorytm sortowania niepochłaniający wiele pamięci polegający na tworzeniu binarnego kopca.<br><ol>Algorytm ten składa się z 2 faz:<li>Początkowo do kopca należy tylko pierwszy element w tablicy. Następnie kopiec rozszerzany jest o drugą, trzecią i kolejne pozycje tablicy, przy czym przy każdym rozszerzeniu, nowy element jest przemieszczany w górę kopca, tak aby spełnione były relacje pomiędzy węzłami.</li><li>Po utworzeniu kopca następuje właściwe sortowanie. Polega ono na usunięciu wierzchołka kopca, zawierającego element maksymalny (minimalny), a następnie wstawieniu w jego miejsce elementu z końca kopca i odtworzenie porządku kopcowego. W zwolnione w ten sposób miejsce, zaraz za końcem zmniejszonego kopca wstawia się usunięty element maksymalny. Operacje te powtarza się aż do wyczerpania elementów w kopcu.</li></ol></span><span class="info">Złożoność czasowa: <b>O(n*log(n))</b></span><br><span class="info">Złożoność pamięciowa: <b>O(n)</b></span><br></div>',
'<div><span class="float opis-margin">Insertion sort (sortowanie prez wstawianie) - jeden z najprostrzych algorytmów sortowania, którego zasada działania odzwierciedla sposób w jaki ustawia się karty - ustawiane są na podstawie pozostałych dobranych kart.<ul>Zalety insertion sort:<li>liczba wykonanych porównań jest zależna od liczby inwersji w permutacji, dlatego algorytm jest wydajny dla danych wstępnie posortowanych</li><li>jest wydajny dla zbiorów o niewielkiej liczebności</li><li>jest stabilny</li></ul><ol>Algorytm składa się z 5 kroków:<li>Utwórz zbiór elementów posortowanych i przenieś do niego dowolny element ze zbioru nieposortowanego.</li><li>Weź dowolny element ze zbioru nieposortowanego.</li><li>Wyciągnięty element porównuj z kolejnymi elementami zbioru posortowanego, póki nie napotkasz elementu równego lub elementu większego (jeśli chcemy otrzymać ciąg niemalejący) lub nie znajdziemy się na początku/końcu zbioru uporządkowanego.</li><li>Wyciągnięty element wstaw w miejsce, gdzie skończyłeś porównywać.</li><li>Jeśli zbiór elementów nieuporządkowanych jest niepusty, wróć do punktu 2.</li></ol></span><span class="info">Złożoność czasowa: <b>O(n^2)</b></span><br><span class="info">Złożoność pamięciowa: <b>O(1)</b></span><br></div>',
'<div><span class="float opis-margin">Shell sort (sortowanie Shella) - jeden z algorytmów sortwania, który można traktować jako uogólnione sortowane przez wstawianie, które dopuszcza zamiany(porównania) elementów położone daleko od siebie. Na początku sortuje on elementy tablicy położone daleko od siebie, a następnie stopniowo zmniejsza odstępy między sortowanymi elementami. Dzięki temu może je przenieść w docelowe położenie szybciej niż zwykłe sortowanie przez wstawianie. Pierwszą wersję tego algorytmu, której zawdzięcza on swoją nazwę, opublikował w 1959 roku Donald Shell. Złożoność czasowa sortowania Shella w dużej mierze zależy od użytego w nim ciągu odstępów. Wyznaczenie jej dla wielu stosowanych w praktyce wariantów tego algorytmu pozostaje problemem otwartym. Algorytm jest wieloprzebiegowy i zależy od implementacji programisty jeżeli chodzi o odstępy porównywania.</span><span class="info">Złożoność czasowa: <b>zależy od ciągu odstępów</b></span><br><span class="info">Złożoność pamięciowa: <b>O(1)</b></span><br></div>',
'<div><span class="float opis-margin">Quick sort (sortowanie szybkie) - algorytm sortowania stosujący metodę "dziel i zwyciężaj". Z tablicy wybiera się element rozdzielający, po czym tablica jest dzielona na dwa fragmenty: do początkowego przenoszone są wszystkie elementy nie większe od rozdzielającego, do końcowego wszystkie większe. Potem sortuje się osobno początkową i końcową część tablicy. Rekursja kończy się, gdy kolejny fragment uzyskany z podziału zawiera pojedynczy element, jako że jednoelementowa tablica nie wymaga sortowania.<br></span><span class="info">Złożoność czasowa: <b>O(n*log(n)), pesymistycznie O(n^2)</b></span><br><span class="info">Złożoność pamięciowa: <b>O(n*log(n)), pesymistycznie O(n^2)</b></span><br></div>'
];

function clearButton(){
	userData = [];
	data.innerHTML = "[ ]";
	output.innerHTML = "";
}
function sortButton(){
	let timer0 = performance.now();
	let arr = [].concat(userData)
	let sort = sortingAlgorithms[place](arr);
	let timer1 = performance.now();
	let time = timer1-timer0;
	if (time === 0) time = "<0.001";
	output.innerHTML = `Posortowana tablica: ${sort.sortedArray}<br>Czas pracy: ${time}ms`;
	output.innerHTML+=`<br>Ilość operacji porównań: ${sort.comps}`;
	if (place !== 1) output.innerHTML+=`<br>Ilość zamian elementów: ${sort.swaps}`;
	else output.innerHTML+=`<br>Ilość scaleń: ${sort.merges}`;
	if (place === 5) output.innerHTML+=`<br>Wartości pivota: ${sort.pivots}`;
	if (place === 4) output.innerHTML+=`<br>Wartości przyrostu: ${sort.gaps}`;
}

let rodzajeTablic = ["losowa", "rosnąca", "malejąca", "A-kształtna", "V-kształtna"];
function testButton(){
	let ns = nsFromUser;
	let m = hmInput.value;
	let testArrays = genTestArrays(ns, m);
	let how = rawData.checked; 
	testingOutput.innerHTML = "";
	testArrays.forEach(testsArray => {
		let size = testsArray[0].array.length;
		testingOutput.innerHTML += `<h1>Sortowanie tablic o rozmiarze ${size}</h1><br>`;
		let outputDiv = document.createElement("div");
		outputDiv.classList.add("testing-output");
		for(let algo = 1;algo<6;++algo){
			let div = document.createElement("div");
			div.classList.add("output");
			div.innerHTML += `<h2 style="text-align: center; margin: 0;">${sortingAlgorithmsNames[algo]}</h2><br>`;
			testsArray.forEach(testArray => {
				let {array: sortMe, policy: policy} = testArray;
				try {
					div.innerHTML += `${size} ${getTestResults(algo, sortMe, how)}`;
				}
				catch(err){
					div.innerHTML += err + '<br>';
				}
				if (!how) div.innerHTML += `Tablica ${rodzajeTablic[policy]}<br><br>`;
				else div.innerHTML += ` ${policy}` + '<br>';
			});
			outputDiv.appendChild(div);
		}
		testingOutput.appendChild(outputDiv);
		});
}

function clearTestingButton(){
	nsFromUser = [];
	testingNOutput.innerHTML = "[ ]";
	//output.innerHTML = "";
}
nsInput.addEventListener("keyup", e => {
	if (e.key === "Enter"){
		let inp = parseInt(nsInput.value);
		if (!isNaN(inp)){
			nsFromUser.push(inp);
			testingNOutput.innerHTML = `[ ${nsFromUser} ]`;
			nsInput.value = "";
			nsInputText.style.color = "white";
		}
		else{
			nsInputText.style.color = "red";
		}
	}
});
input.addEventListener("keyup", e => {
	if (e.key === "Enter"){
		let inp = parseInt(input.value);
		if (!isNaN(inp)){
			userData.push(inp);
			data.innerHTML = `[ ${userData} ]`;
			input.value = "";
			dataInputText.innerHTML = "Wprowadź dane";
			if (userData.length === 10){
				dataInputText.innerHTML = "Wprowadzono 10 liczb do posortowania";
				input.style.display = "none";
			}
		}
		else{
			dataInputText.innerHTML = "Wprowadź poprawne dane";
		}
	}
});
let implementationBox = document.querySelector(".algo-right");//(".implementation-container");
let codeBox = document.querySelector(".implementation");
function update(){
	output.innerHTML = "";
	algorithmName.innerHTML = sortingAlgorithmsNames[place];
	if (!place) {
		algoLeft.style.width = "100%";
		implementationBox.style.display = "none";
		particularAlgorithm.style.display = "none";
		testingDiv.style.display = "block";
		testingNOutput.innerHTML = `[ ${nsFromUser} ]`;
		hmInput.value = hmFromUser;
		return;
	}
	algoLeft.style.width = "";
	testingDiv.style.display = "none";
	particularAlgorithm.style.display = "block";
	implementationBox.style.display = "block";
	//Updating the algotithm description
	opis.innerHTML = opisy[place];
	//Updating the code in CodeBox
    while (codeBox.firstChild) {
        codeBox.removeChild(codeBox.firstChild);
    }
    implementations[place].forEach(line => {
        let span = document.createElement('span');
        span.innerHTML = line;
        span.classList.add("code-line");
        codeBox.appendChild(span);
    });
}
update();
